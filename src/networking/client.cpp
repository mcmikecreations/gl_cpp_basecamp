#include "networking/client.hpp"
#include "networking/client_impl.hpp"

int client::get_port() { return _port; }

client::~client(){}

std::unique_ptr<client> client::create()
{
  return std::make_unique<details::client_impl>();
}