#include "networking/server.hpp"
#include "networking/server_impl.hpp"

int server::get_port() { return _port; }

server::~server(){}

std::unique_ptr<server> server::create()
{
  return std::make_unique<details::server_impl>();
}