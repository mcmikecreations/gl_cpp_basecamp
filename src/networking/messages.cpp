#include <string>
#include "networking/messages.hpp"

message hello_request()
{
  message result;
  std::string payload = "0000: Hello!";
  result.data = std::vector<std::uint8_t>(payload.begin(), payload.end());
  result.data.push_back(0);
  return result;
}

message directory_search_request(const char *name)
{
  message result;
  std::string payload = "0C00: ";
  payload += name;
  result.data = std::vector<std::uint8_t>(payload.begin(), payload.end());
  result.data.push_back(0);
  return result;
}

message directory_search_ping()
{
  message result;
  std::string payload = "0C01: processing";
  result.data = std::vector<std::uint8_t>(payload.begin(), payload.end());
  result.data.push_back(0);
  return result;
}

message directory_search_response(bool success, const char *fullPath)
{
  message result;
  std::string payload = "0C02: ";
  if (success)
  {
    payload += "Success! ";
    payload += fullPath;
  }
  else
  {
    payload += "Failure!";
  }
  result.data = std::vector<std::uint8_t>(payload.begin(), payload.end());
  result.data.push_back(0);
  return result;
}
