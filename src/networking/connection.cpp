#include "networking/connection.hpp"
#include "networking/connection_impl.hpp"

connection::~connection(){}

std::unique_ptr<connection> connection::create(std::uint64_t receiver_socket)
{
  return std::make_unique<details::connection_impl>(receiver_socket);
}