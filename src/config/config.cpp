#include "config/config.hpp"
#include "config/config_impl.hpp"

std::unique_ptr<config> config::create()
{
  return std::make_unique<details::config_impl>();
}