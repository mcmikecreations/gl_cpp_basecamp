#include <iostream>
#include "config/config.hpp"
#include "networking/server.hpp"
#include "networking/network_constants.hpp"

int main(int argc, char** argv)
{
   auto config = config::create();
   if (argc >= 2)
   {
      if (config->load_settings(argv[1]) == false) return 1;
   }
   else
   {
      return 1;
   }
   auto server = server::create();
   if (server->start(
      config->get_setting(KEY_SERVER_PORT)
      ) == false)  return 1;
   if (server->stop() == false) return 1;
   return 0;
}