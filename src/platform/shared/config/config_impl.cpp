#include <iostream>
#include <fstream>
#include <string>
#include "config/config_impl.hpp"
using namespace details;

const char *config_impl::get_setting(const char *key)
{
  auto it = _values.find(key);
  if (it == _values.end()) return nullptr;
  return it->second.c_str();
}

bool config_impl::load_settings(const char *path)
{
  _values.clear();
  std::string line;
  std::ifstream file(path);
  if (!file.is_open()) return false;

  while (!file.eof())
  {
    std::getline(file, line);
    std::size_t delim;
    // Remove comments.
    delim = line.find(';');
    if (delim != std::string::npos)
    {
      line = line.substr(0, delim);
    }
    // Find value
    delim = line.find('=');
    if (delim == std::string::npos) continue;
    _values[line.substr(0, delim)] = line.substr(delim + 1);
  }
  file.close();

  return true;
}
