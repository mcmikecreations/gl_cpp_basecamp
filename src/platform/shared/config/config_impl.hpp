#pragma once

#include <map>
#include <string>
#include "api.hpp"
#include "config/config.hpp"

namespace details
{
  class GL_APITYPE config_impl : public config
  {
  public:
    const char *get_setting(const char *key) override;
    bool load_settings(const char *path) override;
  private:
    std::map<std::string, std::string> _values;
  };
}