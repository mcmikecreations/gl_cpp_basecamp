#pragma once
#include <string>
#include "api.hpp"
#include "networking/connection.hpp"

namespace details
{
  class GL_APITYPE connection_impl : public connection
  {
  public:
    connection_impl(std::uint64_t receiver_socket);

    bool send(const message &data, int flags) override;
    bool receive(message &data, int flags) override;
    bool loop() override;
  private:
  };
}
