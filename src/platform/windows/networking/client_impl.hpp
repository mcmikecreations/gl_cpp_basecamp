#pragma once
#include <memory>
#include <string>
#include "api.hpp"
#include "networking/client.hpp"
#include "networking/connection.hpp"

namespace details
{
  class GL_APITYPE client_impl : public client
  {
  public:
    bool start(const char *serverIp, const char *serverPort) override;
    bool stop() override;
  private:
    std::unique_ptr<connection> _connection;
    std::uint64_t _socket;
  };
} // namespace details
