#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include "networking/message.hpp"
#include "networking/messages.hpp"
#include "networking/server_impl.hpp"
#include "networking/socket_impl.hpp"
#pragma comment(lib, "Ws2_32.lib")
using namespace details;

bool server_impl::start(const char *serverPort)
{
  if (winsock_init() == false) return false;

  int iResult;
  struct addrinfo *result = NULL,
                  *ptr = NULL,
                  hints;

  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;
  hints.ai_flags = AI_PASSIVE;

  // Resolve the server address and port
  iResult = getaddrinfo(NULL, serverPort, &hints, &result);
  if (iResult != 0) {
      printf("getaddrinfo failed: %d\r\n", iResult);
      stop();
      return false;
  }

  _socket = INVALID_SOCKET;

  // Create a SOCKET for connecting to server
  _socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
  if (_socket == INVALID_SOCKET) {
    printf("socket failed with error: %ld\r\n", WSAGetLastError());
    freeaddrinfo(result);
    stop();
    return false;
  }

  // Setup the TCP listening socket
  iResult = bind( _socket, result->ai_addr, (int)result->ai_addrlen);
  if (iResult == SOCKET_ERROR) {
    printf("bind failed with error: %d\r\n", WSAGetLastError());
    freeaddrinfo(result);
    closesocket(_socket);
    stop();
    return false;
  }

  freeaddrinfo(result);

  iResult = listen(_socket, SOMAXCONN);
  if (iResult == SOCKET_ERROR) {
    printf("listen failed with error: %d\r\n", WSAGetLastError());
    closesocket(_socket);
    stop();
    return false;
  }

  // TODO: add parallelism here.
  do
  {
    // Accept a client socket
    SOCKET ClientSocket = accept(_socket, NULL, NULL);
    if (ClientSocket == INVALID_SOCKET) {
        printf("accept failed with error: %d\r\n", WSAGetLastError());
        stop();
        return false;
    }
    _connection = connection::create(ClientSocket);
    message msg;
    
    // The task itself:
    if (_connection->receive(msg, 0) == false)
    {
      closesocket(ClientSocket);
      break;
    }
    printf("%s\r\n", (const char *)msg.data.data());
    
    msg = directory_search_request("Programs");
    if (_connection->send(msg, 0) == false)
    {
      closesocket(ClientSocket);
      break;
    }
    
    while (_connection->receive(msg, 0) == true)
    {
      printf("%s\r\n", (const char *)msg.data.data());
    }

    closesocket(ClientSocket);
  } while (false);

  stop();
  return true;
}

bool server_impl::stop()
{
  int iResult;
  // cleanup
  if (_socket != INVALID_SOCKET)
  {
    iResult = closesocket(_socket);
    if (iResult == SOCKET_ERROR) {
      printf("socket close failed with error: %ld\r\n", WSAGetLastError());
    }
    _socket = INVALID_SOCKET;
  }
  return winsock_term();
}
