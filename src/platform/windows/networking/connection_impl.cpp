#include <winsock2.h>
#include <ws2tcpip.h>
#include <algorithm>
#include <iostream>
#include "networking/connection_impl.hpp"
#pragma comment(lib, "Ws2_32.lib")
using namespace details;

#define BUFFER_SIZE 512

connection_impl::connection_impl(std::uint64_t receiver_socket)
{
  _receiver_socket = receiver_socket;
}

bool connection_impl::send(const message &data, int flags)
{
  int iResult;
  iResult = ::send( _receiver_socket, (const char *)data.data.data(), min( (int)data.data.size(), BUFFER_SIZE ), 0 );
  if (iResult == SOCKET_ERROR) {
    printf("send failed with error: %d\r\n", WSAGetLastError());
    return false;
  }
  return true;
}

bool connection_impl::receive(message &data, int flags)
{
  int iResult;
  std::uint8_t buffer[BUFFER_SIZE] = {};
  iResult = ::recv(_receiver_socket, (char *)buffer, sizeof(buffer), 0);
  if ( iResult > 0 )
  {
    data.data = std::vector<std::uint8_t>(buffer, buffer + iResult);
    return true;
  }
  else if ( iResult == 0 )
  {
    printf("Connection closed\r\n");
    return false;
  }
  else
  {
    printf("recv failed with error: %d\r\n", WSAGetLastError());
    return false;
  }
}

bool connection_impl::loop()
{
 return true;
}
