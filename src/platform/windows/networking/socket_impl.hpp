#pragma once

#include <mutex>
#include "api.hpp"

namespace details
{
  bool winsock_init();
  bool winsock_term();
}