#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include "networking/socket_impl.hpp"
#pragma comment(lib, "Ws2_32.lib")

static std::mutex winsock_mutex;
static std::size_t winsock_users = 0;

bool details::winsock_init()
{
  const std::lock_guard<std::mutex> lock(winsock_mutex);
  ++winsock_users;

  if (winsock_users > 1) return true;

  WSADATA wsaData;
  int iResult;
  // Initialize Winsock
  iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
  if (iResult != 0) {
      printf("WSAStartup failed: %d\n", iResult);
      winsock_users = 0;
      return false;
  }

  return true;
}

bool details::winsock_term()
{
  const std::lock_guard<std::mutex> lock(winsock_mutex);
  if (winsock_users == 0) return true;
  --winsock_users;
  if (winsock_users > 0) return true;

  int iResult = WSACleanup();
  if (iResult == SOCKET_ERROR) {
    printf("WSACleanup failed: %ld\n", WSAGetLastError());
    return false;
  }

  return true;
}
