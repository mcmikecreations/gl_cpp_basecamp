#pragma once
#include <memory>
#include <string>
#include <vector>
#include "api.hpp"
#include "networking/server.hpp"
#include "networking/connection.hpp"

namespace details
{
  class GL_APITYPE server_impl : public server
  {
  public:
    bool start(const char *serverPort) override;
    bool stop() override;
  private:
    std::unique_ptr<connection> _connection;
    std::uint64_t _socket;
  };
} // namespace details
