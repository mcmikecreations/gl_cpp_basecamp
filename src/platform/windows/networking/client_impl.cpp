#include <winsock2.h>
#include <ws2tcpip.h>
#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>
#include "networking/message.hpp"
#include "networking/messages.hpp"
#include "networking/client_impl.hpp"
#include "networking/socket_impl.hpp"
#pragma comment(lib, "Ws2_32.lib")
using namespace details;
using namespace std::chrono_literals;

#define DIR_SIZE 2048

void find_folder(const wchar_t *path, std::string &output, std::atomic_bool &finished)
{
  HANDLE hFind;
  WIN32_FIND_DATAW data;

  std::wstring mask(DIR_SIZE, L'\0');
  GetCurrentDirectoryW(mask.size(), &mask[0]);
  std::wstring folder(DIR_SIZE, L'\0');
  GetVolumePathNameW(mask.c_str(), &folder[0], folder.size());

  mask.erase(mask.find(L'\0'));
  folder.erase(folder.find(L'\0'));
  folder += L"**";
  folder += std::wstring(path).substr(6);
  folder += L"\\";
  printf("Folder to search: %ls\r\n", folder.c_str());
  
  hFind = FindFirstFileW(mask.c_str(), &data);
  if(hFind!=INVALID_HANDLE_VALUE)
  {
      do
      {
          std::wstring name( folder );
          name += data.cFileName;
          if ( ( data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
                // I see you don't want FILE_ATTRIBUTE_REPARSE_POINT
                && !( data.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT ) )
          {
              // Skip . and .. pseudo folders.
              if ( wcscmp( data.cFileName, L"." ) != 0 && wcscmp( data.cFileName, L".." ) != 0 )
              {
                printf("Found: %ls\r\n", name.c_str());
              }
          }
      } while( FindNextFileW(hFind, &data) );
  }
  FindClose(hFind);

  output = "MyFolder";
  finished.store(true);
}

bool client_impl::start(const char *serverIp, const char *serverPort)
{
  if (winsock_init() == false) return false;

  int iResult;
  struct addrinfo *result = NULL,
                  *ptr = NULL,
                  hints;

  ZeroMemory( &hints, sizeof(hints) );
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  // Resolve the server address and port
  iResult = getaddrinfo(serverIp, serverPort, &hints, &result);
  if (iResult != 0) {
      printf("getaddrinfo failed: %d\r\n", iResult);
      stop();
      return false;
  }

  _socket = INVALID_SOCKET;

  // Attempt to connect to an address until one succeeds
  for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

    // Create a SOCKET for connecting to server
    _socket = socket(ptr->ai_family, ptr->ai_socktype, 
      ptr->ai_protocol);
    if (_socket == INVALID_SOCKET) {
      printf("socket failed with error: %ld\r\n", WSAGetLastError());
      freeaddrinfo(result);
      stop();
      return false;
    }

    // Connect to server.
    iResult = connect( _socket, ptr->ai_addr, (int)ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
      closesocket(_socket);
      _socket = INVALID_SOCKET;
      continue;
    }
    break;
  }

  freeaddrinfo(result);

  if (_socket == INVALID_SOCKET) {
    printf("Unable to connect to server!\r\n");
    stop();
    return false;
  }

  _connection = connection::create(_socket);
  message msg;

  // The task itself:
  // Send hello
  msg = hello_request();
  if (_connection->send(msg, 0) == false)
  {
    stop();
    return false;
  }
  {
    // Receive path
    if (_connection->receive(msg, 0) == false)
    {
      stop();
      return false;
    }
    printf("%s\r\n", (const char *)msg.data.data());

    std::wstring path(msg.data.size(), L' ');
    std::size_t charsConverted;
    ::mbstowcs_s(&charsConverted, &path[0], path.size(), (const char *)msg.data.data(), msg.data.size());
    path.resize(charsConverted);
    std::string output;
    std::atomic_bool findFinished(false);
    std::thread worker(find_folder, path.c_str(), std::ref(output), std::ref(findFinished));
    msg = directory_search_ping();
    while (findFinished.load() == false)
    {
      std::this_thread::sleep_for(500ms);
      // Send ping
      if (_connection->send(msg, 0) == false)
      {
        stop();
        return false;
      }
    }
    worker.join();
    if (output == "")
    {
      msg = directory_search_response(false, nullptr);
    }
    else
    {
      msg = directory_search_response(true, output.c_str());
    }
    // Send result
    if (_connection->send(msg, 0) == false)
    {
      stop();
      return false;
    }
  }

  stop();
  return true;
}

bool client_impl::stop()
{
  int iResult;
  // cleanup
  if (_socket != INVALID_SOCKET)
  {
    iResult = closesocket(_socket);
    if (iResult == SOCKET_ERROR) {
      printf("socket close failed with error: %ld\r\n", WSAGetLastError());
    }
    _socket = INVALID_SOCKET;
  }
  return winsock_term();
}
