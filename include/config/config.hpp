#pragma once

#include <memory>
#include "api.hpp"

class GL_APITYPE config
{
public:
  virtual const char *get_setting(const char *key) = 0;
  virtual bool load_settings(const char *path) = 0;
  
  static std::unique_ptr<config> create();
};