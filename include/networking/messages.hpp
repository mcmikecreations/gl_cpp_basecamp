#pragma once

#include "networking/message.hpp"

message hello_request();
message directory_search_request(const char *name);
message directory_search_ping();
message directory_search_response(bool success, const char *fullPath);
