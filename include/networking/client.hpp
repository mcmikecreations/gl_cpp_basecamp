#pragma once

#include <memory>
#include "api.hpp"

class GL_APITYPE client
{
public:
  virtual bool start(const char *serverIp, const char *serverPort) = 0;
  virtual bool stop() = 0;

  int get_port();

  virtual ~client();
  static std::unique_ptr<client> create();
protected:
  int _port;
};