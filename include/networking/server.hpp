#pragma once

#include <memory>
#include "api.hpp"

class GL_APITYPE server
{
public:
  virtual bool start(const char *serverPort) = 0;
  virtual bool stop() = 0;

  int get_port();

  virtual ~server();
  static std::unique_ptr<server> create();
protected:
  int _port;
};