#pragma once

#include <memory>
#include "api.hpp"
#include "networking/message.hpp"

class GL_APITYPE connection
{
public:
  virtual bool send(const message &data, int flags) = 0;
  virtual bool receive(message &data, int flags) = 0;
  virtual bool loop() = 0;

  virtual ~connection();
  static std::unique_ptr<connection> create(std::uint64_t receiver_socket);
protected:
  std::uint64_t _receiver_socket;
};
